package io.muic.cloud.assn2;

import com.amazonaws.SdkClientException;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.AmazonS3Exception;

/**
 * Created by panteparak on 5/9/17.
 */
public interface S3Utils {

  default AmazonS3 createConnection() throws SdkClientException, AmazonS3Exception {
    return AmazonS3ClientBuilder.standard()
            .withCredentials(new ProfileCredentialsProvider())
            .withRegion(Regions.AP_SOUTHEAST_1)
            .build();
  }

  void createBucket (AmazonS3 s3Client, String bucketName);
  void addObjectToBucket(AmazonS3 s3Client, String bucketName, String keyName, String filePath);
  void viewObjectsInBucket(AmazonS3 s3Client, String bucketName);
  void deleteBucket(AmazonS3 s3Client, String bucketName);

}
