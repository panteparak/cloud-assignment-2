package io.muic.cloud.assn2;

import com.amazonaws.SdkClientException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.*;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.TransferManagerBuilder;
import com.amazonaws.services.s3.transfer.Upload;

import java.io.File;
import java.text.MessageFormat;

/**
 * Created by panteparak on 5/9/17.
 */
public class S3UtilsImpl implements S3Utils {

  @Override
  public void createBucket(AmazonS3 s3Client, String bucketName){
    try {
      if (!s3Client.doesBucketExist(bucketName)){
        s3Client.createBucket(bucketName);
        System.out.println(bucketName + " created");
      }
      else System.out.println("bucket: " + bucketName + " already exist.");

    } catch (SdkClientException e){
      System.out.println("Error: " + e.getMessage());
    } catch (Exception e){
      System.out.println("Error: " + e.getMessage());
    }

  }

  @Override
  public void addObjectToBucket(AmazonS3 s3Client, String bucketName, String keyName, String filePath) {

    try {
      File file = new File(filePath);
      if (!file.isFile()) System.out.println("Specified path is not a file");
      else {
        TransferManager transferManager = TransferManagerBuilder.standard()
                .withS3Client(s3Client)
                .build();

        Upload upload = transferManager.upload(bucketName, keyName, file);
        upload.waitForUploadResult();
        System.out.println("upload completed");

      }
    } catch (SdkClientException e){
      System.out.println("Error: " + e.getMessage());
    } catch (InterruptedException e){
      System.out.println("Upload failed");
    }
  }

  @Override
  public void viewObjectsInBucket(AmazonS3 s3Client, String bucketName) {

    try {
      if (!s3Client.doesBucketExist(bucketName)) System.out.println("Specified bucket not found");
      else {
        ListObjectsV2Result result;
        ListObjectsV2Request request = new ListObjectsV2Request().withBucketName(bucketName);

        final String FORMAT = "object key: {0} | size: {1} bytes | last-modified: {2} | storage class: {3}";
        MessageFormat messageFormat = new MessageFormat(FORMAT);

        do {
          result = s3Client.listObjectsV2(request);
          for (S3ObjectSummary file : result.getObjectSummaries()){
            String out = messageFormat.format(new String[]{
                    file.getKey(),
                    String.valueOf(file.getSize()),
                    file.getLastModified().toString(),
                    file.getStorageClass()
            });
            System.out.println(out);
          }

          request.setContinuationToken(result.getNextContinuationToken());
        }while (result.isTruncated());
      }
    } catch (SdkClientException e){
      System.out.println("Error: " + e.getMessage());
    } catch (Exception e){
      System.out.println("Error: " + e.getMessage());
    }

  }

  @Override
  public void deleteBucket(AmazonS3 s3Client, String bucketName) {
    try {
      if (!s3Client.doesBucketExist(bucketName)){
        System.out.println("Specified bucket not found");
      } else {
        ListObjectsV2Result result;
        ListObjectsV2Request request = new ListObjectsV2Request().withBucketName(bucketName);

        do {
          result = s3Client.listObjectsV2(request);

          for (S3ObjectSummary file : result.getObjectSummaries())
            s3Client.deleteObject(file.getBucketName(), file.getKey());

          request.setContinuationToken(result.getNextContinuationToken());
        }while (result.isTruncated());

        ListVersionsRequest versionsRequest = new ListVersionsRequest().withBucketName(bucketName);
        VersionListing versionListing;

        do {
          versionListing = s3Client.listVersions(versionsRequest);
          for (S3VersionSummary file : versionListing.getVersionSummaries())
            s3Client.deleteVersion(file.getBucketName(), file.getKey(), file.getVersionId());
        }while (versionListing.isTruncated());

        s3Client.deleteBucket(bucketName);
        System.out.println(bucketName + " deleted");
      }
    } catch (SdkClientException e){
      System.out.println("Error: " + e.getMessage());
    } catch (Exception e){
      System.out.println("Error: " + e.getMessage());
    }
  }
}
