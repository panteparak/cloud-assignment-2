# ICCS 486 Cloud Computing #

## S3 Storage Manager ##

### How to Run ###
* Make sure `Maven build-tool is installed`
* Edit the `Main` class to call the wanted method.
* Go to root directory of project, run command `mvn package` to run the task or use your favourite IDE to run the program.